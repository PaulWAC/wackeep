@extends('layouts.master')

@section('content')
<div class="add_note">+</div>
<div class="modal">
<h2>Note ta note</h2>	
	{!! Form::open(['action' => 'NoteController@store','id' => 'form_note']) !!}
		{!! Form::textarea('text','',['class' => 'text', 'id' => 'text']) !!}
		{!! Form::submit('Create',['class' => 'create']) !!}
		{!! Form::close() !!}
</div>
<?php 
	foreach($notes as $note):
 ?>
<div class=".col-md-4">
	<section class="tasks">
	    <header class="tasks-header">
	      <h3 class="tasks-title">Notes {!! $note->id !!}</h3>
	      <a href="#" class="tasks-lists">Lists</a>
	      	<div class="sub-menu-tasks">
	      		<a href="#">Ajouter une tâche</a>
	      		<a href="#">Supprimer la note</a>
	      	</div>
	    </header>
		{!! Form::open(['action' => 'NoteController@update']) !!}
		{!! Form::textarea('text',$note->text,['class' => 'text', 'id' => 'text','data-note_id' => $note->id]) !!}
		{!! Form::submit('Save',['class' => 'update']) !!}
		{!! Form::close() !!}
	</section>
</div>
<?php 
	endforeach; 
?>
@endsection