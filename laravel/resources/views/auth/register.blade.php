@extends('layouts.master')

@section('content')
<div class=".col-md-6 .col-md-offset-3">
    <div class="container">
        <form method="POST" action="/auth/register">
            {!! csrf_field() !!}

            <div>
                Name
                <input type="text" name="name" value="{{ old('name') }}" class="form-control">
            </div>

            <div>
                Email
                <input type="email" name="email" value="{{ old('email') }}" class="form-control">
            </div>

            <div>
                Password
                <input type="password" name="password" class="form-control">
            </div>

            <div>
                Confirm Password
                <input type="password" name="password_confirmation" class="form-control">
            </div>

            <div>
                <button class="btn btn-default" type="submit">Register</button>
            </div>
        </form>
    </div>
</div>
@endsection