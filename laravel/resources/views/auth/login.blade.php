@extends('layouts.master')

@section('content')
<div class=".col-md-6 .col-md-offset-3">
    <div class="container">
        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}

            <div>
                Email
                <input type="email" name="email" value="{{ old('email') }}" class="form-control">
            </div>

            <div>
                Password
                <input type="password" name="password" id="password" class="form-control">
            </div>

            <div>
                <input type="checkbox" name="remember"> Remember Me
            </div>

            <div>
                <button type="submit" class="btn btn-default">Login</button>
            </div>
        </form>
    </div>
</div>
@endsection