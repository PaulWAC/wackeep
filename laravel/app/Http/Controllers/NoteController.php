<?php

namespace App\Http\Controllers;

use App\Note;
use Input;
use Illuminate\Http\Request as Request;
use App\Http\Controllers\Controller;


class NoteController extends Controller
{
    /**
     * Create a new note instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Current user
        $user = $request->user();

        $note = new Note;
        $note->user_id = $user->id;
        $note->color = '';
        $note->text = $request->input('text');

        if($note->save())
        {
            return redirect('/home');
        }
        else
        {
            return redirect('/home');
        }
    }

    public function update(Request $request, $id){
        $data = Input::all();
        if($request->ajax())
        {
            $note = Note::find($id);
            $note->text = $data['text'];
            $note->save();
        }
       
    }

    public function index()
    {
        $notes = Note::all();
        return view('home.index', ['notes' => $notes]);
    }
}