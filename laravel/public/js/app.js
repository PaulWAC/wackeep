$(function() {

// UPLOAD NOTES AJAX

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.update').click(function(e) {
    e.preventDefault();

        var url             = "http://localhost:8000/post";
        var $notes             = {};
        $notes.id            = $(this).parent().find('textarea').data('note_id');
        $notes.text            = $(this).parent().find('textarea').val();

        $.ajax({
        type: "POST",
        url: url + '/' + $notes.id,
        data: { _method: 'PUT', text: $notes.text },
        cache: false,
        success: function(data){
           return data;
        }
        });
        return false;
    });

// ADD NEW NOTES

$('.add_note').click(function(e){
	e.preventDefault();
	$('.modal').css('display','block');
})


});